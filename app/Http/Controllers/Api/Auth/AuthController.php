<?php
/**
 * Created by PhpStorm.
 * User: anandia
 * Date: 1/8/18
 * Time: 8:30 PM
 */

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Lcobucci\JWT\Parser;
use Illuminate\Http\Request;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function logout(Request $request) {
        $value = $request->bearerToken();
        if ($value) {

            $id = (new Parser())->parse($value)->getHeader('jti');
            $revoked = DB::table('oauth_access_tokens')->where('id', '=', $id)->update(['revoked' => 1]);
            //$this->guard()->logout();
        }
        return Response(['code' => 200, 'message' => 'You are successfully logged out'], 200);
    }
}